package de.hbrs.liquidservice;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.hbrs.wirschiffendas.data.entity.*;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;

import static de.hbrs.wirschiffendas.data.entity.AlgorithmIdentifier.LIQUID;

@SpringBootApplication
@RestController
@EnableAsync
public class LiquidServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(LiquidServiceApplication.class, args);
    }

    @Async
    @PostMapping(value = "/checkConfig", consumes = "application/json")
    public void checkConfig(@RequestBody String json) {
        Configuration conf = null;
        try {
            conf = new ObjectMapper().readValue(json, Configuration.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        System.out.println("before sleep");

        try {
            Thread.sleep(10 * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("after sleep");

        sendSuccess();

        System.out.println("after send");


    }

    @Async
    void sendSuccess() {
        Status status = Status.READY;
        StatusTransferItem statusTransferItem = new StatusTransferItem(LIQUID, status);
        String statusUrl = "http://localhost:8080/statusUpdate";

        RestTemplate restTemplate = new RestTemplate();
        try {
            URI uri = new URI(statusUrl);
            ResponseEntity<StatusTransferItem> responseEntity = restTemplate.postForEntity(uri, statusTransferItem, StatusTransferItem.class);
            System.out.println(responseEntity.getStatusCode());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        ResultTransferItem resultTransferItem = new ResultTransferItem(Result.SUCCESS, LIQUID);
        String resultUrl = "http://localhost:8080/resultUpdate";

        RestTemplate resultRestTemplate = new RestTemplate();
        try {
            URI uri = new URI(resultUrl);
            ResponseEntity<ResultTransferItem> responseEntity = resultRestTemplate.postForEntity(uri, resultTransferItem, ResultTransferItem.class);
            System.out.println(responseEntity.getStatusCode());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

    }
}
